class CreateUserSports < ActiveRecord::Migration
  def change
    create_table :user_sports do |t|
      t.integer :sport_id
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
