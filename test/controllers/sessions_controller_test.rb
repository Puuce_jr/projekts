require 'test_helper'

class SessionsControllerTest < ActionController::TestCase
  test "should get new" do
    get :new
    assert_response :success
  end

  test "should get create" do
    get :create
    assert_response :redirect
  end

  test "should get destroy" do
    get :destroy
    assert_response :redirect
  end

  test "Should login" do
    dave = users(:one)
    post :create, username: dave.username, password_digest: 'secret'
    assert_redirected_to news_index_path
    assert_equal dave.id, session[:user_id]
  end

  test "should fail login" do
    dave = users(:one)
    post :create, username: dave.username, password_digest: 'wrong'
    assert_redirected_to login_url
  end

end
