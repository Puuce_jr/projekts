class ApplicationController < ActionController::Base
  before_filter :authorize
  before_action :set_sport

  def authorize
    unless User.find_by(id: session[:user_id]==1)
      redirect_to news_index_path
    end
  end

  def set_sport
    @sports = Sport.all.order('sports ASC')
    @izv_sport = Sport.joins(user_sports: :user).where(users: {:id => (session[:user_id])})
  end

  def current_user
  	User.find(session[:user_id])
  	session[:user_id]=User.id
  	user
	end

end
