class UserSportsController < ApplicationController
  include CurrentUser
  skip_before_action :authorize, only: [:create, :delete, :destroy]
  before_action :set_user_sport, only: [:delete, :create1]
  before_action :set_user_sport, only: [:show, :edit, :update, :destroy]
  # GET /user_sports
  # GET /user_sports.json
  def index
    @user_sports = UserSport.all
  end


  # GET /user_sports/1
  # GET /user_sports/1.json
  def show
    @user_sport = UserSport.find(params[:id])

  end

  # GET /user_sports/new
  def new
    @user_sport = UserSport.new
    @user_sport1 = UserSport.new
  end

  # GET /user_sports/1/edit
  def edit
    @user_sport = UserSport.find(params[:id])
  end
  def create
  # POST /user_sports
  # POST /user_sports.json
    if session[:user_id]==1
      sport = Sport.find_by_id(params[:sport_id])
      user = User.find(session[:user_id])
      if UserSport.new
        @user_sport = UserSport.new(user_sport_params)
        respond_to do |format|
          if @user_sport.save
            format.html { redirect_to user_sports_path}
            format.json { render :show, status: :created, location: @user_sport }
            else
            format.html { render :new}
            format.json { render json: @user_sport.errors, status: :unprocessable_entity }
          end
        end
        else
          sport = Sport.find_by_id(params[:sport_id])
          user = User.find(session[:user_id])
          @user_sport1 = UserSport.new(sport_id: sport.id, user_id: user.id)
              respond_to do |format|
                if @user_sport.save
                  format.html { redirect_to news_index_path}
                  format.json { render :show, status: :created, location: @user_sport }
                  else
                    format.html { redirect_to news_index_path}
                    format.json { render json: @user_sport.errors, status: :unprocessable_entity }
                end
              end
        end
    else
      sport = Sport.find_by_id(params[:sport_id])
      user = User.find(session[:user_id])
      @user_sport = UserSport.new(sport_id: sport.id, user_id: user.id)
        respond_to do |format|
        if @user_sport.save
          format.html { redirect_to news_index_path}
          format.json { render :show, status: :created, location: @user_sport }
          else
            format.html { redirect_to news_index_path}
            format.json { render json: @user_sport.errors, status: :unprocessable_entity }
        end
       end
     end
  end

  # PATCH/PUT /user_sports/1
  # PATCH/PUT /user_sports/1.json
  def update
    @user_sport = UserSport.find(params[:id])
    respond_to do |format|
      if @user_sport.update(user_sport_params)
        format.html { redirect_to news_index_path, notice: 'User sport was successfully updated.' }
        format.json { render :show, status: :ok, location: @user_sport }
      else
        format.html { render :edit }
        format.json { render json: @user_sport.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /user_sports/1
  # DELETE /user_sports/1.json
  def destroy
       respond_to do |format|
    if session[:user_id] == 1
      if UserSport.find_by_id(params[:id])
          @user_sport = UserSport.find(params[:id])
          @user_sport.destroy
          format.html { redirect_to news_index_path}
          format.json { head :no_content }
      else
        @user_sport = UserSport.find_by_sport_id_and_user_id(params[:id], session[:user_id])
        @user_sport.destroy
          format.html { redirect_to user_sports_path}
          format.json { head :no_content }
      end
    elsif session[:user_id]
      @user_sport = UserSport.find_by_sport_id_and_user_id(params[:id], session[:user_id])
      @user_sport.destroy
        format.html { redirect_to news_index_path}
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_sport
     sport = Sport.find_by_id(params[:sport_id])
     user = User.find(session[:user_id])
      @user_sport = UserSport.find_by_sport_id_and_user_id(sport, user)
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def user_sport_params
      params.require(:user_sport).permit(:sport_id, :user_id)
    end
end
