class MyProfilesController < ApplicationController
  before_action :set_my_profile, only: [:show, :edit, :update, :destroy]
  skip_before_action :authorize, only: [:edit, :show]
  # GET /my_profiles
  # GET /my_profiles.json
  def index
    
  end

  # GET /my_profiles/1
  # GET /my_profiles/1.json
  def show
      redirect_to news_index_path
  end

  # GET /my_profiles/new
  def new
  end

  # GET /my_profiles/1/edit
  def edit
    @user = User.find(session[:user_id])
  end

  # POST /my_profiles
  # POST /my_profiles.json
  def create
    @my_profile = MyProfile.new(my_profile_params)

    respond_to do |format|
      if @my_profile.save
        format.html { redirect_to @my_profile, notice: 'My profile was successfully created.' }
        format.json { render :show, status: :created, location: @my_profile }
      else
        format.html { render :new }
        format.json { render json: @my_profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /my_profiles/1
  # PATCH/PUT /my_profiles/1.json
  def update
    respond_to do |format|
      if @users.update(user_params)
        format.html { redirect_to @my_profile, notice: 'My profile was successfully updated.' }
        format.json { render :show, status: :ok, location: @my_profile }
      else
        format.html { render :edit }
        format.json { render json: @my_profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /my_profiles/1
  # DELETE /my_profiles/1.json
  def destroy
    @my_profile.destroy
    respond_to do |format|
      format.html { redirect_to my_profiles_url, notice: 'My profile was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_my_profile
      @user = User.find(session[:user_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def my_profile_params
      params[:my_profile]
    end
end
