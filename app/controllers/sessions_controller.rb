class SessionsController < ApplicationController
  skip_before_action :authorize
  
  def create
  	user = User.find_by_username(params[:username])
  	if user and user.authenticate(params[:password_digest])
  		session[:user_id] = user.id
  		session[:username] = user.username
  		redirect_to news_index_path
  	else
  		redirect_to login_url, alert: "Lietotāja vārda un paroles kombinācija nesakrīt"
  	end
  end

  def destroy
  	session[:user_id] = nil
  	redirect_to login_url, notice: "Izrakstījāties"
  end

  def new
    unless session[:user_id] 
      @user = User.new
    else
      redirect_to news_index_path 
    end
  end
end
