jQuery ->
  if $('#infinite-scrolling').size() > 0
    $('#news_page').on 'scroll', ->
      url = $('.pagination a.next_page').attr('href')
      if url && $(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight
        $('.pagination').html('<img src="/assets/ajax-loader.gif" alt="Loading..." title="Loading..." />')
        $.getScript url
      return
  return