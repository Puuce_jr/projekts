class Feed < ActiveRecord::Base
	SPORTS = [
	["Hokejs", 				1],
	["Basketbols", 			2],
	["Futbols", 			4],
	["Volejbols", 			5],	
	["Florbols", 			6],
	["Handbols", 			7],
	["Motoru sports", 		12],
	["Vieglatlētika", 		16],
	["Riteņbraukšana",		17],
	["Pludmales volejbols",	18],
	["Orientēšanās",		19],
	["Ziemas sports",		20],
	["Pokers",				21],
	["Peldēšana",			22],
	["Regbijs",				23],
	["Bokss",				24],
	["Telpu futbols",		25],
	["Golfs",				26],
	["Cīņas sports",		27],
	["Spēka sports",		28],
	["Ekstrēmais sports",	29] ]
	belongs_to :sport
	validates  :url, :sport_id, presence: true
end