class UserSport < ActiveRecord::Base
	validates  :user_id, :sport_id, presence: true
	validates_uniqueness_of  :user_id, :scope => :sport_id

	belongs_to :user
	belongs_to :sport
	has_many :feed_entries
end