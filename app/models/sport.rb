class Sport < ActiveRecord::Base
	validates :sports, presence: true, uniqueness: true
	has_many :users, through: :user_sports
	has_many :feed_entries
	has_many :user_sports
	has_many :feeds
end
