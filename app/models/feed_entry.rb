class FeedEntry < ActiveRecord::Base  
  def feed_entry_params
    params.require(:feed_entrys).permit(:guid, :name, :published_at, :summary, :url, :sport_id)
  end
  
  belongs_to :sport
  has_many :users
  has_many :user_sports

  def self.update_from_feed(feed_url)
    feed = Feedjira::Feed.fetch_and_parse(feed_url)  
    add_entries(feed.entries)  
  end 

  def self.update_from_feed_continuously  
    Feed.all.each do |feed1|

      feed = Feedjira::Feed.fetch_and_parse(feed1.url)  
      add_entries(feed.entries, feed1.sport_id)  
    end  
  end 

private  

  def self.add_entries(entries, sport_id)
      entries.each do |entry|  
        unless exists? :guid => entry.id  
          create!(  
            :name         => entry.title,  
            :summary      => entry.summary,  
            :url          => entry.url,  
            :published_at => entry.published,  
            :guid         => entry.id, 
            :sport_id     => sport_id, 
          ) 
        
      end  
    end  
  end  
end 
 










