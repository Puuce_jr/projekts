class User < ActiveRecord::Base
  validates :username, presence: true, uniqueness: true
  validates :news_amount, presence: true
  has_secure_password
  has_many :sports, through: :user_sports
  has_many :user_sports
  has_many :feed_entries ,through: :user_sports, through: :sports
end
 