json.array!(@my_profiles) do |my_profile|
  json.extract! my_profile, :id
  json.url my_profile_url(my_profile, format: :json)
end
