json.array!(@user_sports) do |user_sport|
  json.extract! user_sport, :id, :sport_id, :user_id
  json.url user_sport_url(user_sport, format: :json)
end
