json.array!(@feeds) do |feed|
  json.extract! feed, :id, :url, :sport_id
  json.url feed_url(feed, format: :json)
end
