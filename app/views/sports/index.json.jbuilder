json.array!(@sports) do |sport|
  json.extract! sport, :id, :sports
  json.url sport_url(sport, format: :json)
end
